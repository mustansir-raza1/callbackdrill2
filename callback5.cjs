// Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

//     Get information from the Thanos boards
//     Get all the lists for the Thanos board
//     Get all cards for the Mind and Space lists simultaneously

const fs = require("fs");
const path = require("path");
const callback1 = require("./callback1.cjs")
const callback2 = require("./callback2.cjs");
const callback3 = require("./callback3.cjs");

const boardPath = path.join(__dirname, "boards.json");
const listPath = path.join(__dirname, "lists_1.json");
const cardsPath = path.join(__dirname, "cards.json");

function thanosInfo() {
    setTimeout(() => {
        const thanos = "mcu453ed";
        callback1(boardPath, thanos, (err, data) => {
            if (err) {
                console.log(err)
            }
            else {
                console.log("information of thanos board:", data)
            }
        })
        callback2(listPath, thanos, (err, data) => {
            if (err) {
                console.log("err")
            }
            else {
                console.log("list of thanos:", data)
            }
        })
        const mind = "qwsa221"
        const space = "jwkh245"
        callback3(cardsPath, mind, (err, data) => {
            if (err) {
                console.error(err)
            }
            else {
                console.log(`mind cards:`, data)
            }
        })
        callback3(cardsPath, space, (err, data) => {
            if (err) {
                console.error(err)
            }
            else {
                console.log(`space cards:`, data)
            }
        })

    }, 2000)
}
module.exports = thanosInfo;
