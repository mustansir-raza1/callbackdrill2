// Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.


const fs = require("fs")
function boardInfo (boardPath, boardId, callback){
    setTimeout(()=>{
    fs.readFile(boardPath, "utf-8", (err, data)=>{
        if(err){
            console.error(err);
        }
        else{
            const boards = JSON.parse(data)
            const foundBoard = boards.find(board => board.id === boardId)
            
            if(foundBoard){
                callback(null,foundBoard);
            }else{
                callback(new Error("Didn't fount id!",null));
            }

        }
    })
},2000);
}
module.exports = boardInfo;