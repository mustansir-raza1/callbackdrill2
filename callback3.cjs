//Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.

const fs = require('fs');
const path = require("path")
function cardId (cardsPath,listId,callback){
    setTimeout(()=>{
        // const cardsPath = path.join(__dirname, 'cards.json')
        fs.readFile(cardsPath, "utf-8", (err, data)=>{
            if(err){
                console.error(err)
            }
            else{
                const cardId = JSON.parse(data);
                const cardsFound = cardId[listId]
                if(cardsFound){
                    callback(null,cardsFound)
                }
                else{
                    callback(new Error("didnt found it",null))
                }
            }
        })
    }, 2000)
}
//cardId("qwsa221")
module.exports = cardId;