// Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

//     Get information from the Thanos boards
//     Get all the lists for the Thanos board
//     Get all cards for all lists simultaneously

const fs = require("fs");
const callback1 = require("./callback1.cjs")
const callback2 = require("./callback2.cjs")
const callback3 = require("./callback3.cjs")

const path = require("path");
const boardPath = path.join(__dirname, "boards.json")
const listPath = path.join(__dirname, "lists_1.json")
const cardsPath = path.join(__dirname, "cards.json")

function thanosInfo() {
    setTimeout(() => {
        const thanos = 'mcu453ed'
        callback1(boardPath, thanos, (err, data) => {
            if (err) {
                console.error(err)
            }
            else {
                console.log("information of thanos:", data)
            }
            callback2(listPath, data.id, (err, data) => {
                if (err) {
                    console.error(err)
                }
                else {
                    console.log(data)
                }
                for (let list in data) {
                    setTimeout(() => {
                        callback3(cardsPath, data[list].id, (err,data)=>{
                            if(err){
                                console.log("Data not found")
                            }
                            else{
                                console.log(data)
                            }
                        })


                    }, 2000)


                }
            })
        })
}, 2000)
}
module.exports = thanosInfo;