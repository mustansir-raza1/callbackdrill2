// Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

//     Get information from the Thanos boards
//     Get all the lists for the Thanos board
//     Get all cards for the Mind list simultaneously
const callback1 = require("./callback1.cjs");
const callback2 = require("./callback2.cjs");
const callback3 = require("./callback3.cjs");
const fs = require("fs");
const path = require("path")
// const callback1 = path.join(__dirname, "callback1.cjs");
const boardPath = path.join(__dirname, "boards.json");

function thanosInfo() {

    setTimeout(() => {

        const thanos = "mcu453ed";

        callback1(boardPath, thanos, (err, data) => {
            if (err) {
                console.log(err)
            }
            else {
                console.log("information of thanos board:", data)
            }
        })
        const listPath = path.join(__dirname,"lists_1.json");
        callback2(listPath, thanos, (err, data) => {
            if (err) {
                console.error(err)
            }
            else {
                console.log("list for thanos board:", data)
            }
        })

        const cardsPath = path.join(__dirname, "cards.json");
        const mind = "qwsa221"
        callback3(cardsPath, mind, (err, data) => {
            if (err) {
                console.error("error")
            }
            else {
                console.log(data)
            }
        })
    }, 3000)


}
module.exports = thanosInfo;


