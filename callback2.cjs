// Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.

const fs = require("fs");
const path = require("path")
function listInfo(listPath,boardID, callback) {
    setTimeout(() => {
        fs.readFile(listPath, "utf-8", (err, data) => {
            if (err) {
                console.error(err)
            }
            else {
                const list = JSON.parse(data);
                const listFound = list[boardID];
                if(listFound){
                    callback(null,listFound)
                }
                else{
                    callback(new Error(`didnt found it`, null))
                }
            }
        })
    },2000)
}
//listInfo("mcu453ed")
module.exports = listInfo;